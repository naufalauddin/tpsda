import java.util.Scanner;
import java.util.HashMap;


public class TeamTree {
    static Scanner in = new Scanner(System.in);
    static String[] donatPilihan;
    static HashMap<String, Wilayah> peta = new HashMap<>();
    static Wilayah[] provinceMap;

    public static void main(String[] args) {
        readInput();

        inputOrder();
        // testAVL();
    }

    public static void readInput() {
        donatPilihan = in.nextLine().split(" ");
        int jmlNonKelurahan = Integer.parseInt(in.nextLine());

        String[] relation = in.nextLine().split(" ");
        
        Wilayah nation = new Wilayah(relation[0]);
        peta.put(relation[0], nation);
        int provinceAmount = Integer.parseInt(relation[1]);
        provinceMap = new Wilayah[provinceAmount];

        // get province
        for (int i = 2; i < relation.length; i++) {
            String namaProvince = relation[i];
            Wilayah province = new Wilayah(namaProvince, nation);
            provinceMap[i-2] = province;
            peta.put(namaProvince, province);
        }

        // get the rest of subwilayah
        for (int i = 0; i < jmlNonKelurahan-1; i++) {
            relation = in.nextLine().split(" ");
            for (int j = 2; j < relation.length; j++) {
                Wilayah superWilayah = peta.get(relation[0]);
                String namaWilayah = relation[j];
                Wilayah wilayah = new Wilayah(namaWilayah, superWilayah);
                peta.put(namaWilayah, wilayah);
            }
        }
        
        // System.out.println(Wilayah.statusMenang[0]+' '+Wilayah.statusMenang[1]);
    }


    public static void inputOrder() {
        int jumlahPerintah = Integer.parseInt(in.nextLine());
        String[] perintah;
        
        for (int i = 0; i < jumlahPerintah; i++) {
            perintah = in.nextLine().split(" ");
            long jumlahVoteA;
            long jumlahVoteB;
            int minimal;
            long minimalSelisih;
            String namaWilayah;
            String namaKandidat;
            // System.out.println(Wilayah.treeDifference);
            switch(perintah[0]) {
                case "TAMBAH":
                    jumlahVoteA = Long.parseLong(perintah[2]);
                    jumlahVoteB = Long.parseLong(perintah[3]);
                    namaWilayah = perintah[1];
                    addVote(namaWilayah, jumlahVoteA, jumlahVoteB);
                    break;
                case "ANULIR":
                    jumlahVoteA = Long.parseLong(perintah[2]);
                    jumlahVoteB = Long.parseLong(perintah[3]);
                    namaWilayah = perintah[1];
                    substractVote(namaWilayah, jumlahVoteA, jumlahVoteB);
                    break;
                case "CEK_SUARA":
                    namaWilayah = perintah[1];
                    cekWilayah(namaWilayah);
                    break;
                case "WILAYAH_MENANG":
                    namaKandidat = perintah[1];
                    wilayahMenang(namaKandidat);
                    break;
                case "CEK_SUARA_PROVINSI":
                    for (int j = 0; j < provinceMap.length; j++) {
                        System.out.println(provinceMap[j]);
                    }
                    break;
                case "WILAYAH_MINIMAL":
                    namaKandidat = perintah[1];
                    minimal = Integer.parseInt(perintah[2]);
                    wilayahMinimal(namaKandidat, minimal);
                    break;
                case "WILAYAH_SELISIH":
                    minimalSelisih = Long.parseLong(perintah[1]);
                    wilayahSelisih(minimalSelisih);
                    break;
            }
            // System.out.println("after");
            // System.out.println(Wilayah.treeDifference);
        }
    }
    
    public static void addVote(String wilayah, long pilihan1, long pilihan2) {
        peta.get(wilayah).addVote(pilihan1, pilihan2);
    }

    public static void substractVote(String wilayah, long pilihan1, long pilihan2) {
        peta.get(wilayah).substractVote(pilihan1, pilihan2);
    }

    public static void cekWilayah(String namaWilayah) {
        Wilayah wilayah = peta.get(namaWilayah);
        System.out.print(wilayah.getPilihan1());
        System.out.print(" ");
        System.out.println(wilayah.getPilihan2());
    }

    public static void wilayahMenang(String namaKandidat) {
        if (namaKandidat == donatPilihan[0]) {
            System.out.println(Wilayah.statusMenang[0]);
        } else {
            System.out.println(Wilayah.statusMenang[1]);
        }
    }

    public static void wilayahMinimal(String namaKandidat, int presentase) {
        long jumlahMinimal = 0l;
        if (namaKandidat == donatPilihan[0]) {
            for (int i = presentase; i < 101; i++) {
                jumlahMinimal += Wilayah.presentaseA[i];
            }
        } else {
            for (int i = presentase; i < 101; i++) {
                jumlahMinimal += Wilayah.presentaseB[i];
            }
        }
        System.out.println(jumlahMinimal);
    }

    public static void wilayahSelisih(long minimal) {
        long jumlahMinimal = Wilayah.treeDifference.getMinimalWeight(minimal);
        System.out.println(jumlahMinimal);
    }

    public static void testAVL() {
        Scanner in = new Scanner(System.in);
    
        String[] insert = in.nextLine().split(" ");
        WeightedAVLTree<Integer> a = new WeightedAVLTree<Integer>(Integer.valueOf(insert[0]));
        
        for (int i = 1; i < insert.length; i++) {
            a.insert(Integer.valueOf(insert[i]));
            System.out.println(a);
        }
    
        insert = in.nextLine().split(" ");
        
        for (int i = 0; i < insert.length; i++) {
            a.substract(Integer.valueOf(insert[i]));
            System.out.println(a);
        }    
    }
}

/**
 * A node of WeightedAVLTree used to store left and right node and data in WeightedAVLTree.
 * 
 * @param <T> any type of comparable data that will be stored in the node.
 */
class WeightedAVLNode<K extends Comparable<K>> {

    protected WeightedAVLNode<K> left;  // left node of the node
    protected WeightedAVLNode<K> right; // right node of the node
    protected int height;               // height of the sub tree
    
    protected int weight;               // data stored in the node.
    protected K key;                    // data stored in the node.

    protected int weightLeft;
    protected int weightRight;

    /**
     * A Tree node with weight. If data to be inserted already in the tree,
     * weight of the node for corresponding data will be added.
     * <br></br>
     * Great for AVLTree with a duplicated data.
     * 
     * @param key , data stored in the node
     */
    public WeightedAVLNode(K key) {
        
        this.key = key;
        this.weight = 1;

        this.height = 0;
    }

    /**
     * Update the node's height by finding the highest children's height
     * and adding it by one.
     */
    public void updateHeight() {
        if (left == null && right == null) {
            height = 0;
        } else if (left == null) {
            height = right.height + 1;
        } else if (right == null) {
            height = left.height + 1;
        } else {
            height = Math.max(left.height, right.height) + 1;
        }
    }

    public void updateChildWeight() {
        if (left == null && right == null) {
            weightLeft = 0;
            weightRight = 0;
        } else if (left == null) {
            weightLeft = 0;
            weightRight = right.weight;
        } else if (right == null) {
            weightRight = 0;
            weightLeft = left.weight;
        } else {
            weightLeft = left.weight + left.weightLeft + left.weightRight;
            weightRight = right.weight + right.weightLeft + right.weightRight;
        }
    }

    /**
     * Node will be turned to string as a pair of key and weight.
     * Like <code>(key, weight)</code>
     */
    public String toString() {
        return "(" + key.toString() +", "+weight+")"+"^"+height+"|"+weightLeft+"|"+weightRight; 
    }
}

/**
 * A self balancing Binary Search Tree with weighted node.
 * 
 * @param <K> class of data stored in the tree
 */
class WeightedAVLTree<K extends Comparable<K>> {

    private WeightedAVLNode<K> root;

    private WeightedAVLNode<K> temp;


    public WeightedAVLTree() {
        this.root = null;
    }

    /**
     *  
     * @param data data stored in the tree
     */
    public WeightedAVLTree(K key) {
        root = new WeightedAVLNode<K>(key);
    }

    /**
     * Inserting data into the tree. Inserted data will be compared to data in the root.
     * If <code> key.compareTo(root.key) > 0 </code> then key will be inserted into the right subtree.
     * If <code> key.compareTo(root.key) < 0 </code> then key will be inserted into the left subtree.
     * If <code> key.compareTo(root.key) == 0 </code> then weight of the root will be incremented.
     * <br></br>
     * However if <code> root </code> is <code> null </code> then create a new node as the root.
     * 
     * @param root of the subtree
     * @param key to be inserted
     * @return the root of the sub tree
     */
    public WeightedAVLNode<K> insert(WeightedAVLNode<K> root, K key) {
        if (root == null) {
            return new WeightedAVLNode<K>(key);
        }

        if (root.key.compareTo(key) == 0) {
            root.weight++;
            return root;
        }

        // if data is bigger than the current root.
        if (root.key.compareTo(key) < 0) {
            
            root.right = insert(root.right, key);
            
            root = balance(root);

        // if data is smaller than the root.
        } else {
            
            root.left = insert(root.left, key);
            root = balance(root);

        }

        root.updateHeight();
        root.updateChildWeight();
        return root;
    }   
    
    /**
     * insert a data to the tree.
     * 
     * @param key data to be inserted
     */
    public void insert(K key) {
        root = insert(root, key);
    }

    /**
     * Substract weight of the given key in the tree. If weight of the given key
     * reaches zero, then remove the key from the tree.
     * 
     * @param subroot subroot of tree to be substracted
     * @param key to be substracted
     * @return subroot after substracting.
     */
    public WeightedAVLNode<K> substract(WeightedAVLNode<K> subroot, K key) {
        if (subroot == null) return subroot;
        if (subroot.key.compareTo(key) == 0) {
            if (subroot.weight > 1) {
                subroot.weight--;
                return subroot;
            } else {
            //     if (subroot.left == null) return subroot.right;
            //     if (subroot.left.right != null) {
            //         subroot.left = removeMaxChild(subroot.left);
            //         temp.left = subroot.left;
            //         temp.right = subroot.right;
            //         subroot = temp;
            //     } else {
            //         subroot.left.right = subroot.right;
            //         subroot = subroot.left;
            //     }
            //     subroot.updateHeight();
            //     subroot.updateChildWeight();
            //     subroot = balance(subroot);
                subroot.weight = 0;
                return subroot;
            }
        } else if (subroot.key.compareTo(key) < 0) {
            subroot.right = substract(subroot.right, key);
            subroot.updateHeight();
            subroot.updateChildWeight();
            subroot = balance(subroot);
            return subroot;
        } else {
            subroot.left = substract(subroot.left, key);
            subroot.updateHeight();
            subroot.updateChildWeight();
            subroot = balance(subroot);
            return subroot;
        }
    }
    
    /**
     * 
     * @param key
     */
    public void substract(K key) {
        this.root = substract(this.root, key);
    }
    
    // public WeightedAVLNode<K> removeMaxChild(WeightedAVLNode<K> subroot) {
    //     if (subroot.right.right == null) {
    //         temp = subroot.right;
    //         subroot.right = temp.left;
    //         subroot.updateHeight();
    //         subroot.updateChildWeight();
    //         subroot = balance(subroot);
    //         return subroot;
    //     } else {
    //         subroot.right = removeMaxChild(subroot.right);
    //         subroot.updateHeight();
    //         subroot.updateChildWeight();
    //         subroot = balance(subroot);
    //         return subroot;
    //     }
    // }

    /**
     * 
     * @param subroot
     * @return
     */
    public WeightedAVLNode<K> balance(WeightedAVLNode<K> subroot) {
        if (subroot == null) return subroot;
        if (subroot.left == null && subroot.right == null) return subroot;
        if (subroot.left == null && subroot.right.height == 0) return subroot;
        if (subroot.right == null && subroot.left.height == 0) return subroot;

        if (subroot.left ==  null && subroot.right.height == 1) {

            if (subroot.right.left == null) {
                subroot = singleRightRotation(subroot);

                return subroot;
            }
            else {
                subroot.right = singleLeftRotation(subroot.right);

                subroot = singleRightRotation(subroot);

                return subroot;
            }

        } else if (subroot.right == null && subroot.left.height == 1) {
            if (subroot.left.right == null) {
                subroot = singleLeftRotation(subroot);
                // subroot.updateChildWeight();
                // subroot.updateHeight();
                return subroot;
            }
            else {
                subroot.left = singleRightRotation(subroot.left);
                subroot = singleLeftRotation(subroot);
                // subroot.updateChildWeight();
                // subroot.updateHeight();
                return subroot;
            }
        // if right is higher than left by 2 rotate with right
        } else if (subroot.right.height - subroot.left.height == 2) {
            // if right-right subtree is higher than right-left.
            if (subroot.right.right.height > subroot.right.left.height) {

                // single rotate.
                subroot = singleRightRotation(subroot);
                
            // if right-left subtree is higher than right-right.
            } else {

                // double rotate.
                subroot.right = singleLeftRotation(subroot.right);
                subroot = singleRightRotation(subroot);
            }

        // left is higher than right by 2 rotate with left
        } else if (subroot.left.height - subroot.right.height == 2) {
            // if left-left is higher than left-right
            if(subroot.left.left.height > subroot.left.right.height) {
                
                // single rotate.
                subroot = singleLeftRotation(subroot);
            
            // if left-right is higher than left-left
            } else {

                // double rotate
                subroot.left = singleRightRotation(subroot.left);
                subroot = singleLeftRotation(subroot);
            }
        } 

        subroot.updateHeight();
        subroot.updateChildWeight();
        return subroot;
    }

    /**
     * 
     * @param key
     * @return
     */
    public long getMinimalWeight(K key) {
        return getMinimalWeight(root, key);
    }

    /**
     * 
     * @param subroot
     * @param key
     * @return
     */
    public long getMinimalWeight(WeightedAVLNode<K> subroot, K key) {
        if (subroot == null) return 0;
        if (subroot.key.compareTo(key) < 0) {
            return getMinimalWeight(subroot.right, key);
        } else if (subroot.key.compareTo(key) == 0) {
            return subroot.weight + subroot.weightRight;
        } else {
            return subroot.weight + subroot.weightRight + getMinimalWeight(subroot.left, key);
        }
    }

    /**
     * rotate the subroot with its left child. Update height for
     * both subroot and its right child.
     * 
     * @param root to be rotated
     */
    public WeightedAVLNode<K> singleRightRotation(WeightedAVLNode<K> root) {
        WeightedAVLNode<K> right = root.right;
        
        root.right = right.left;

        right.left = root;

        root.updateHeight();
        right.updateHeight();
        root.updateChildWeight();
        right.updateChildWeight();

        return right;
    }


    /**
     * rotate root with its right child. Update height for both root
     * and its left child.
     * 
     * @param root to be rotated
     */
    public WeightedAVLNode<K> singleLeftRotation(WeightedAVLNode<K> root) {
        WeightedAVLNode<K> left = root.left;

        root.left = left.right;

        left.right = root;

        root.updateHeight();
        left.updateHeight();
        root.updateChildWeight();
        left.updateChildWeight();

        return left;
    }

    public String toString(WeightedAVLNode<K> root) {
        if (root == null) {
            return "null";
        }
        
        String rootString = "<"+ root + " " + root.left + " " + root.right + ">";
        String leftString = "<"+toString(root.left)+">";
        String rightString = "<"+toString(root.right)+">";

        return "{"+rootString+" "+leftString+" "+rightString+"}";
    }

    public String toString() {
        return toString(root);
    }
}

class Wilayah {
    private long pilihan1;
    private long pilihan2;
    private Wilayah superWilayah;
    private String nama; 

    static WeightedAVLTree<Long> treeDifference = new WeightedAVLTree<Long>();
    static long[] statusMenang = new long[2];
    
    static long[] presentaseA = new long[101];
    static long[] presentaseB = new long[101];

    public Wilayah(String nama, long pilihan1, long pilihan2) {
        this.nama = nama;
        this.pilihan1 = pilihan1;
        this.pilihan2 = pilihan2;
        treeDifference.insert(Math.abs(pilihan1 - pilihan2));
        addPresentase();
        addStatusMenang();
    }

    public Wilayah(String nama) {
        this(nama, 0l, 0l);
    }

    public Wilayah(String nama, Wilayah superWilayah) {
        this(nama, 0l, 0l);
        this.superWilayah = superWilayah;
    }

    public void addVote(long pilihan1, long pilihan2) {

        treeDifference.substract(Math.abs(this.pilihan1 - this.pilihan2));
        substractPresentase();
        substractStatusMenang();

        this.pilihan1 += pilihan1;
        this.pilihan2 += pilihan2;

        treeDifference.insert(Math.abs(this.pilihan1 - this.pilihan2));
        addPresentase();
        addStatusMenang();

        if (superWilayah != null) superWilayah.addVote(pilihan1, pilihan2);
    }

    public void substractVote(long pilihan1, long pilihan2) {
        
        treeDifference.substract(Math.abs(this.pilihan1 - this.pilihan2));
        substractPresentase();
        substractStatusMenang();
        
        this.pilihan1 -= pilihan1;
        this.pilihan2 -= pilihan2;

        treeDifference.insert(Math.abs(this.pilihan1 - this.pilihan2));
        addPresentase();
        addStatusMenang();
        
        if (superWilayah != null) superWilayah.substractVote(pilihan1, pilihan2);
    }

    public void addPresentase() {
        if (this.pilihan1 + this.pilihan2 == 0) {
            presentaseA[50]++;
            presentaseB[50]++;
        } else {
            int presentase1 = (int) (pilihan1 * 100 / (pilihan1 + pilihan2));
            int presentase2 = (int) (pilihan2 * 100 / (pilihan1 + pilihan2));
            presentaseA[presentase1]++;
            presentaseB[presentase2]++;
        }
    }

    public void substractPresentase() {
        if (this.pilihan1 + this.pilihan2 == 0) {
            presentaseA[50]--;
            presentaseB[50]--;
        } else {
            int presentase1 = (int) (pilihan1 * 100 / (pilihan1 + pilihan2));
            int presentase2 = (int) (pilihan2 * 100 / (pilihan1 + pilihan2));
            presentaseA[presentase1]--;
            presentaseB[presentase2]--;
        }
    }

    public void addStatusMenang() {
        if (this.pilihan1 == this.pilihan2) return;
        if (this.pilihan1 > this.pilihan2) {
            // System.out.println("x");
            statusMenang[0]++;
        } else if (this.pilihan2 > this.pilihan1) {
            // System.out.println("y");
            statusMenang[1]++;
        }
    }

    public void substractStatusMenang() {
        if (this.pilihan1 == this.pilihan2) return;
        if (this.pilihan1 > this.pilihan2) {
            statusMenang[0]--;
        } else if (this.pilihan2 > this.pilihan1) {
            statusMenang[1]--;
        }
    }

    public long getDifference() {
        return Math.abs(pilihan1 - pilihan2);
    }

    public String toString() {
        return this.nama+" "+this.pilihan1+" "+this.pilihan2;
    }

    public long getPilihan1() {
        return this.pilihan1;
    }

    public long getPilihan2() {
        return this.pilihan2;
    }
}