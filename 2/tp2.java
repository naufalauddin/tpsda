import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Tp2 {
    private static BufferedReader in;
    private static PrintWriter out;
    private static DonatRack rack = new DonatRack();

    public static void main(String[] args) throws IOException {
        InputStream inputStream = System.in;
        in = new BufferedReader(new InputStreamReader(inputStream), 327678);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        readInput();
        printOutput();
        out.close();
    }

    private static void readInput() throws IOException {
        int numberOfColumn = Integer.parseInt(in.readLine());
        for (int i = 0; i < numberOfColumn; i++) {
            createColumn();
        }
        int numberOfInstruction = Integer.parseInt(in.readLine());
        for (int i = 0; i < numberOfInstruction; i++) {
            doInstruction();
        }
    }

    private static void createColumn() throws IOException {
        String[] row = in.readLine().split(" ");
        ColumnDonat columnDonat = new ColumnDonat(Integer.parseInt(row[1]));
        for (int i = 2; i < row.length; i++) {
            columnDonat.inLast(Integer.parseInt(row[i]));
        }
        rack.inLast(columnDonat);
    }

    private static void doInstruction() throws IOException {
        String[] query = in.readLine().split(" ");
        ColumnDonat retrieved = null;
        ColumnDonat originColumn;
        ColumnDonat destinationColumn;
        // System.out.println(query[0]+" "+query[1]);
        switch(query[0]) {
            case "IN_BACK":
                retrieved = rack.retrieveColumn(Integer.parseInt(query[2])-1);
                retrieved.inLast(Integer.parseInt(query[1]));
//                out.print(" "+query[2]+"\n");
                break;
            case "OUT_BACK":
                retrieved = rack.retrieveColumn(Integer.parseInt(query[1])-1);
                retrieved.outLast();
                break;
            case "IN_FRONT":
                retrieved = rack.retrieveColumn(Integer.parseInt(query[2])-1);
                retrieved.inFirst(Integer.parseInt(query[1]));
 //               out.print(" "+query[2]+"\n");
                break;
            case "OUT_FRONT":
                retrieved = rack.retrieveColumn(Integer.parseInt(query[1])-1);
                retrieved.outFirst();
                break;
            case "MOVE_FRONT":
            case "MOVE_BACK":
                int origin = Integer.parseInt(query[1]);
                int destination = Integer.parseInt(query[2]);
                if (origin < destination) {
                    destinationColumn = rack.retrieveColumn(destination-1);
                    originColumn = rack.retrieveColumn(origin-1);
                } else {
                    originColumn = rack.retrieveColumn(origin-1);
                    destinationColumn = rack.retrieveColumn(destination-1);
                }
                if (query[0].equals("MOVE_BACK")) {
                    destinationColumn.appendLast(originColumn);
                } else {
                    destinationColumn.appendFirst(originColumn);
                }
                retrieved = destinationColumn;
//  //               out.print(" "+query[2]+"\n");
//                 printList(originColumn);
//                 printList(destinationColumn);
                break;
            case "NEW":
                retrieved = new ColumnDonat(Integer.parseInt(query[1]));
                // out.print("\n");
                break;
        }
        // printOutput();
        // System.out.println();
        rack.insertColumnDonat(retrieved);
        // printOutput();
        // System.out.println();
    }
    
    public static void printOutput() {
        NodeList<ColumnDonat> pointer = rack.header.next;
        for(int i = 0; i < rack.size; i++) {
            NodeList<Integer> donat = pointer.data.header.next;
            for (int j = 0; j < pointer.data.size; j++) {
                out.print(donat.data);
                if (donat.next != null) {
                    out.print(" ");
                }
                donat = donat.next;
            }
            out.println();
            pointer = pointer.next;
        }
    }

    public static <T> void printList(NextPrevList<T> list) {
        NodeList<T> pointer = list.header.next;
        for (int i = 0; i < list.size; i++) {
            System.out.print(pointer.data);
            if(pointer.next != null) out.print(" ");
            pointer = pointer.next;
        }
        System.out.println();
    }
}

/**
 * A node class for list with linked after and previous node.
 * A node contains a data, addresses for the next and previous node.
 * @param <T>
 */
class NodeList<T> {
    public T data;
    public NodeList<T> next;
    public NodeList<T> prev;

    public NodeList() {
        this(null, null, null);
    }
    public NodeList(T data) {
        this(data, null, null);
    }
    public NodeList(T data, NodeList<T> next, NodeList<T> prev) {
        this.data = data;
        this.next = next;
        this.prev = prev;
    }
}

/**
 * A list made of NodeList class. For easier acces
 * @param <T>
 */
class NextPrevList<T> {
    protected NodeList<T> header;
    protected NodeList<T> tail;
    protected int size;

    public NextPrevList() {
        header = new NodeList<T>();
        tail = new NodeList<T>();
        size = 0;
    }

    public NextPrevList(T data) {
        this();
        NodeList<T> temp = new NodeList<T>(data);
        header.next = temp;
        tail.prev = temp;
        size++;
    }

    /**
     * input @param data into the first order in the list.
     * It creates a new node then attach it to front of the list.
     */
    public void inFirst(T data) {
        NodeList<T> temp= new NodeList<T>(data, header.next, null);
        if(header.next != null) {
            header.next.prev = temp;
        } else {
            tail.prev = temp;
        }
        header.next = temp;
        size++;
    }
    
    /**
     * retrieve the first data on the list and remove it from the list.
     * @return data of the first node in the list.
     */
    public T outFirst() {
        T firstData = header.next.data;
        header.next = header.next.next;
        if (header.next != null) header.next.prev = null;
        else tail.prev = null;
        size--;
        return firstData;
    }

    /**
     * retrieve data from the list without removing it.
     * @return data of the first node in the list
     */
    public T peekFirst() {
        return header.next.data;
    }

    /**
     * insert @param data to the last order in the list.
     */
    public void inLast(T data) {
        NodeList<T> temp = new NodeList<T>(data, null, tail.prev);
        if (tail.prev != null) tail.prev.next = temp;
        else header.next = temp;
        tail.prev = temp;
        size++;
    }

    /**
     * retrieve the last data from the order in the list and remove it.
     * @return the last data in the list.
     */
    public T outLast() {
        T lastData = tail.prev.data;
        tail.prev = tail.prev.prev;
        if (tail.prev != null) tail.prev.next = null;
        else header.next = null;
        size--;
        return lastData;
    }

    /**
     * retrieve the last data without removing it
     * @return the last data in the list
     */
    public T peekLast() {
        return tail.prev.data;
    }

    /**
     * append @param other list to the end of the list
     */
    public void appendLast(NextPrevList<T> other) {
        this.tail.prev.next = other.header.next;
        other.header.next.prev = this.tail.prev;
        this.tail = other.tail;
        this.size = this.size + other.size;
    }

    public void appendFirst(NextPrevList<T> other) {
        this.header.next.prev = other.tail.prev;
        other.tail.prev.next = this.header.next;
        this.header = other.header;
        this.size = this.size + other.size;
    }

    public boolean isEmpty() {
        return header.next == null;
    }
}

/**
 * a class of donat stacked into a column. It extends NextPrevList to simulate add and
 * remove donat according to query needed.
 */
class ColumnDonat extends NextPrevList<Integer> {

    public ColumnDonat(int firstElement) {
        super(firstElement);
    }
    
    /**
     * compare this column with @param other column by comparing number of chocochip
     * in a donat with the same row.
     * 
     * Comparing start from the first row of donat. If a row found a donat with lesser
     * chocochip than then the other column then the former is smaller. If a column
     * with lesser row has no more donat to be compared, then lesser column is smaller.
     * 
     * @return the difference of the first none zero chocochip row difference.
     * 
     * If this column has lesser donat and every row difference of this donat is zero,
     * @return -1, or else this column has more donat @return 1.
     */
    public int compareTo(ColumnDonat other) {
        NodeList<Integer> thisCurrent = this.header.next;
        NodeList<Integer> otherCurrent = other.header.next;

        // check for every row in column donat
        while (thisCurrent != null && otherCurrent != null) {
            int decr = thisCurrent.data - otherCurrent.data;
            // System.out.println("kurang "+decr);
            if (decr != 0) {
                return decr;
            }
            thisCurrent = thisCurrent.next;
            otherCurrent = otherCurrent.next;
        }

        // return smaller if this column get to null first
        return thisCurrent == null? -1 : 1;
    }
}

class DonatRack extends NextPrevList<ColumnDonat> {

    public void insertColumnDonat(ColumnDonat col) {
        if(col == null) return;
        if(col.isEmpty()) return;
        // if rack is empty put column to the first order in the list
        if (isEmpty()) {
            inFirst(col);
            return;
        }

        NodeList<ColumnDonat> pointer = this.header.next;

        int low = 0;
        int high = this.size;
        int pointerPosition = 0;
        boolean smaller = false;

        // binary search the position to insert
        while (low < high) {
            int middle = (high+low)/2;
            int diff = middle - pointerPosition;
            if (diff > 0) {
                for (int i = pointerPosition; i < middle; i ++) {
                    pointer = pointer.next;
                }
            } else {
                for (int i = pointerPosition; i > middle; i--) {
                    pointer = pointer.prev;
                }
            }
            pointerPosition = middle;
            if (col.compareTo(pointer.data) > 0) {
                low = middle+1;
                pointer = pointer.next;
                pointerPosition++;
            } else {
                high = middle;
            }
        }

        // insert the node list
        if (pointerPosition == this.size) {
            this.inLast(col);
        } else  if (pointerPosition == 0) {
            this.inFirst(col);
        } else {
            NodeList<ColumnDonat> insert = new NodeList<>(col);
            pointer.prev.next = insert; 
            insert.prev = pointer.prev; 
            pointer.prev = insert; insert.next = pointer; 
            this.size++;
        } 
    }
    
    public ColumnDonat retrieveColumn(int index) {
        NodeList<ColumnDonat> pointer = this.header.next;
        for (int i = 0; i < index; i++) {
            pointer = pointer.next;
        }

        if (pointer.next != null) pointer.next.prev = pointer.prev;
        else this.tail.prev = pointer.prev;
        if (pointer.prev != null) pointer.prev.next = pointer.next;
        else this.header.next = pointer.next;
        if (this.size == 1) {
            this.header.next = null;
            this.tail.prev = null;
        }
        this.size--;

        return pointer.data;
    }
}