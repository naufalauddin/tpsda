import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.lang.Comparable;
import java.util.PriorityQueue;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

public class MoreTreePleaseForTeamTree {
    private static BufferedReader in;
    private static PrintWriter out;

    public static void main(String[] args) throws IOException {
        
        InputStream inputStream = System.in;
        in = new BufferedReader(new InputStreamReader(inputStream), 327678);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        mainFlow();

        out.flush();
    }

    public static void mainFlow() throws IOException {
        String[] init = in.readLine().split(" ");

        int N = Integer.parseInt(init[0]);
        int M = Integer.parseInt(init[1]);
        int Q = Integer.parseInt(init[2]);

        Graph graph = createGraph(N, M);

        // do instruction
        for (int i = 0; i < Q; i++) {
            String[] query = in.readLine().split(" ");

            int arg1;
            int arg2;
            
            String instruction = query[0];
            switch(instruction) {
                // OS instruction
                case "OS":
                    System.out.println(graph.os(query));
                    break;
                
                // CCGWD instruction
                case "CCWGD":
                    arg1 = Integer.parseInt(query[1]);
                    arg2 = Integer.parseInt(query[2]);
                    System.out.println(graph.ccwgd(arg1, arg2));
                    break;
                
                // LAOR instruction
                case "LAOR":
                    arg1 = Integer.parseInt(query[1]);
                    arg2 = Integer.parseInt(query[2]);
                    System.out.println(graph.laor(arg1, arg2));
                    break;
                    
                // LAORC instruction
                case "LAORC":
                    arg1 = Integer.parseInt(query[1]);
                    arg2 = Integer.parseInt(query[2]);
                    System.out.println(graph.laorc(arg1, arg2));
                    break;
                
                // SR instruction
                case "SR":
                    arg1 = Integer.parseInt(query[1]);
                    arg2 = Integer.parseInt(query[2]);
                    System.out.println(graph.sr(arg1, arg2));
                    break;

                // SM instruction
                case "SM":
                    arg1 = Integer.parseInt(query[1]);
                    arg2 = Integer.parseInt(query[2]);
                    System.out.println(graph.sm(arg1, arg2));
                    break;
            }
        }

    }

    /**
     * create graph for the main program
     * @param vertexAmount amount of vertex in the graph
     * @param edgeAmount amount of edge in the graph
     * @return graph representation created after reading all the input
     * @throws IOException if {@code BufferedReader} failed to read the input 
     */
    public static Graph createGraph(int vertexAmount, int edgeAmount) throws IOException {
        // init graph
        Graph graph = new Graph(vertexAmount);
        
        // loop all the edge
        for (int i = 0; i < edgeAmount; i++) {
            // read edge
            String[] strEdge = in.readLine().split(" ");

            int from = Integer.parseInt(strEdge[0]);
            int to = Integer.parseInt(strEdge[1]);
            int weight = Integer.parseInt(strEdge[2]);

            // add edge to graph
            graph.addEdge(from, to, weight);
        }

        // System.out.println(graph);

        return graph;
    }
}

class WeightedEdge implements Comparable<WeightedEdge> {
    int weight;
    int destination;

    public WeightedEdge(int destination, int weight) {
        this.weight = weight;
        this.destination = destination;
    }

    public int compareTo(WeightedEdge o) {
        return this.weight - o.weight;
    }

    public String toString() {
        return "<"+destination+" "+weight+">";
    }
}

class Graph {
    int[] vertexList;
    ArrayList<ArrayList<WeightedEdge>> adjEdgeList;
    int VERTEX_AMOUNT;
    final int MOD = 10001;

    public Graph(int jumlahVertex) {
        VERTEX_AMOUNT = jumlahVertex;
        vertexList = new int[VERTEX_AMOUNT+1]; // vertex start at 1, so it ends at Amount
        adjEdgeList = new ArrayList<ArrayList<WeightedEdge>>(); // index 0 will be none

        for (int i = 0; i < VERTEX_AMOUNT+1; i++) {
            adjEdgeList.add(new ArrayList<>());
        }
    }

    /**
     * 
     * @param initial
     * @param destination
     * @param weight
     */
    public void addEdge(int initial, int destination, int weight) {
        // create weighted edge
        WeightedEdge edgeFromInitial = new WeightedEdge(destination, weight);
        WeightedEdge edgeFromDestination = new WeightedEdge(initial, weight);

        // add edge to initial
        adjEdgeList.get(initial).add(edgeFromInitial);

        // add edge to destination
        adjEdgeList.get(destination).add(edgeFromDestination);
    }

    /**
     * 
     */
    public String toString() {
        String str = "";
        for (int i = 1; i < vertexList.length; i++) {
            str += i+": ";
            for (WeightedEdge edge: adjEdgeList.get(i)) {
                str += edge;
            }
            str += "\n";
        };
        return str;
    }

    /**
     * method required to solve os query
     * @param open
     * @return
     */
    public int os(String[] open) {
        boolean[] visited = new boolean[VERTEX_AMOUNT+1];
        int reached = 0;

        for (int i = 1; i < open.length; i++) {
            int store = Integer.parseInt(open[i]);
            
            // skip store if visited
            if (visited[store]) continue;
            
            // start bfsing
            Queue<Integer> queue = new ArrayDeque<Integer>();
            queue.add(store);
            visited[store] = true;

            while(!queue.isEmpty() && reached != VERTEX_AMOUNT) {
                // get vertex to be check
                store = queue.poll();

                // put vertex to reachable list
                reached++;
                
                // get all adjacent store
                ArrayList<WeightedEdge> adjStore = adjEdgeList.get(store);
                
                if (adjStore == null) continue;

                // queue every adjacent edge
                for (int j = 0; j < adjStore.size(); j ++) {
                    WeightedEdge edge = adjStore.get(j);
                    int adj = edge.destination;

                    if (visited[adj]) continue;
                    queue.add(adj);
                    visited[adj] = true;
                }
            }
        }

        return reached;
    }

    /**
     * method to solve ccwgd query
     * @param source
     * @param weight
     * @return
     */
    public int ccwgd(int source, int weight) {
        boolean[] visited = new boolean[VERTEX_AMOUNT+1];
        int reachable = 0;

        Queue<Integer> queue = new ArrayDeque<Integer>();

        // System.out.println("ccwgd:"); // uncomment for debug

        queue.add(source);
        visited[source] = true;
        while (!queue.isEmpty()) {
            int store = queue.poll();

            // System.out.println("current: "+store); // uncomment for debug

            // get adjacent edge
            ArrayList<WeightedEdge> edges = adjEdgeList.get(store);

            // iterate every adjacent edge
            for (WeightedEdge edge: edges) {
                int adj = edge.destination;

                // System.out.print(edge+" "); // uncomment for debug

                // skip unnecessary check
                if (visited[adj]) continue;
                if (edge.weight > weight) continue;

                // add city to reachable
                reachable++;
                visited[adj] = true;

                // put into queue
                queue.add(adj);
            }
        }

        return reachable;
    }

    /**
     * 
     * @param source
     * @param destination
     * @return
     */
    public int laor(int source, int destination) {
        boolean[] visited = new boolean[VERTEX_AMOUNT+1];
        int[] distance = new int[VERTEX_AMOUNT+1];
        Queue<Integer> queue = new ArrayDeque<Integer>();
        
        // init bfs
        distance[source] = 0;
        visited[source] = true;
        queue.add(source);

        // bfsing
        while (!queue.isEmpty()) {
            int store = queue.poll();

            // get adjacent edges
            ArrayList<WeightedEdge> edges = adjEdgeList.get(store);

            for (WeightedEdge edge: edges) {
                int adj = edge.destination;

                // skip unnecessary check
                if (visited[adj]) continue;

                // update distance and queue
                distance[adj] = distance[store] + 1;
                visited[adj] = true;
                queue.add(adj);

                // break if already found the destination
                if (adj == destination) return distance[destination];
            }
        }

        // can't reach destination;
        return -1;
    }

    /**
     * 
     * @param source
     * @param destination
     * @return
     */
    public int laorc(int source, int destination) {
        boolean[] visited = new boolean[VERTEX_AMOUNT+1];
        int[] distance = new int[VERTEX_AMOUNT+1];
        int[] ways = new int[VERTEX_AMOUNT+1];
        Queue<Integer> queue = new ArrayDeque<Integer>();
        
        // init bfs
        distance[source] = 0;
        ways[source] = 1;
        visited[source] = true;
        queue.add(source);

        // bfsing
        while (!queue.isEmpty()) {
            int store = queue.poll();

            // get adjacent edges
            ArrayList<WeightedEdge> edges = adjEdgeList.get(store);

            for (WeightedEdge edge: edges) {
                int adj = edge.destination;
                int dist = distance[store] + 1;

                if (distance[adj] == 0) distance[adj] = dist;

                // skip unnecessary check
                if (dist > distance[adj]) continue;

                ways[adj] += ways[store];
                ways[adj] %= MOD;
          
                // update queue
                if (!visited[adj]) {
                    visited[adj] = true;
                    queue.add(adj);
                }
            }
        }

        return ways[destination];
    }

    /**
     * 
     * @param source
     * @param destination
     * @return
     */
    public int sr(int source, int destination) {
        boolean[] green = new boolean[VERTEX_AMOUNT+1];
        int[] currentDistance = new int[VERTEX_AMOUNT+1];

        // init to make all distance to be infinitly far away
        for (int i = 0; i < currentDistance.length; i++) {
            currentDistance[i] = Integer.MAX_VALUE;
        }

        // init the queue
        PriorityQueue<WeightedEdge> queue = new PriorityQueue<WeightedEdge>();
        WeightedEdge initial = new WeightedEdge(source, 0);
        queue.add(initial);

        while(!queue.isEmpty()) {
            // get minimum distance available from source
            WeightedEdge edge = queue.poll();
            int current = edge.destination;
            int weight = edge.weight;

            // if it is destination returns it
            // already the nearest distance
            if (current == destination) return weight;
            
            // skip if vertex already in green area
            if (green[current]) continue;

            // put to green area
            green[current] = true;
            currentDistance[current] = weight;

            // iterate every edge adjacent to the vertex
            for (WeightedEdge e: adjEdgeList.get(current)) {
                WeightedEdge path = new WeightedEdge(e.destination, e.weight + weight);
                queue.add(path);
            }
        }

        return -1;
    }

    public int sm(int cityA, int cityB) {
        boolean[] green = new boolean[VERTEX_AMOUNT+1];
        int[] currentDistance = new int[VERTEX_AMOUNT+1];

        // init to make all distance to be infinitly far away
        for (int i = 0; i < currentDistance.length; i++) {
            currentDistance[i] = Integer.MAX_VALUE;
        }

        // init the queue
        PriorityQueue<WeightedEdge> queue = new PriorityQueue<WeightedEdge>();
        WeightedEdge initial = new WeightedEdge(cityA, 0);
        queue.add(initial);

        while(!queue.isEmpty()) {
            // get minimum distance available from source
            WeightedEdge edge = queue.poll();
            int current = edge.destination;
            int weight = edge.weight;
            
            // skip if vertex already in green area
            if (green[current]) continue;

            // put to green area
            green[current] = true;
            currentDistance[current] = weight;

            // iterate every edge adjacent to the vertex
            for (WeightedEdge e: adjEdgeList.get(current)) {
                WeightedEdge path = new WeightedEdge(e.destination, e.weight + weight);
                queue.add(path);
            }
        }

        // if currentDistance still infinite, than there are no path
        if (currentDistance[cityB] == Integer.MAX_VALUE) return -1;

        // dijkstark for cityB
        green = new boolean[VERTEX_AMOUNT+1];

        initial = new WeightedEdge(cityB, 0);
        queue.add(initial);
        while(!queue.isEmpty()) {
            // get minimum distance available from source
            WeightedEdge edge = queue.poll();
            int current = edge.destination;
            int weight = edge.weight;

            // skip if vertex already in green area
            if (green[current]) continue;

            // put to green area
            green[current] = true;
            currentDistance[current] = Math.max(weight, currentDistance[current]);

            // iterate every edge adjacent to the vertex
            for (WeightedEdge e: adjEdgeList.get(current)) {
                WeightedEdge path = new WeightedEdge(e.destination, e.weight + weight);
                queue.add(path);
            }
        }

        // find minimum meet up point
        int minimum = currentDistance[0];
        for (int i : currentDistance) {
            minimum = Math.min(minimum, i);
        }
        return minimum;
    }
}